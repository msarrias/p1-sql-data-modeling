import os
import glob
import sqlite3
import json
import pandas

pandas.options.mode.chained_assignment = None  # default='warn'

# from sql_queries import *

def if_none(x):
    if x==None:
        return 'NULL'
    else:
        return(x)

def process_song_file(cur, filepath):
    # open song file
    with open(filepath, 'r') as f:
        d = json.load(f)

    # insert song record
    command = 'INSERT INTO songs VALUES ("{}","{}","{}",{},{}) ON CONFLICT (song_id) DO NOTHING'.format(
        d['song_id'],
        d['title'],
        d['artist_id'],
        d['year'],
        d['duration']
    )
    cur.execute(command)
    
    # insert artist record
    command = 'INSERT INTO artists VALUES ("{}","{}","{}",{},{}) ON CONFLICT (artist_id) DO NOTHING'.format(
        d['artist_id'],
        d['artist_name'],
        d['artist_location'],
        if_none(d['artist_latitude']),
        if_none(d['artist_longitude'])
    )
    cur.execute(command)


def process_log_file(cur, filepath):

    docs = []
    with open(filepath) as f:
        lines = f.readlines()
    for line in lines:
        j = json.loads(line)
        docs.append(j)

    df = pandas.DataFrame(docs)
    df = df[df['page']=='NextSong']
    df['ts2'] = df['ts']*1000000
    df['time'] = pandas.to_datetime(df['ts2'])
    dft = df[['ts','time']]
    dft['hour'] = df['time'].dt.hour
    dft['day'] = df['time'].dt.day
    dft['month'] = df['time'].dt.month
    dft['year'] = df['time'].dt.year
    dft['week'] = df['time'].dt.isocalendar().week
    dft['weekday'] = df['time'].dt.weekday

    for index, row in dft.iterrows():
        command = 'INSERT INTO time VALUES ({},{},{},{},{},{},{}) ON CONFLICT (start_time) DO NOTHING'.format(
            row['ts'],
            row['day'],
            row['month'],
            row['year'],
            row['hour'],
            row['week'],
            row['weekday'],
            )
        cur.execute(command)

    # load user table
    dfu = df[['userId','firstName','lastName','gender','level']]
    dfu = dfu.drop_duplicates()
    dfu['userId'] = pandas.to_numeric(dfu['userId'])

    for index, row in dfu.iterrows():
        command = 'INSERT INTO users VALUES ({},"{}","{}","{}","{}") ON CONFLICT (user_id) DO UPDATE SET level  = EXCLUDED.level'.format(
            row['userId'],
            row['firstName'],
            row['lastName'],
            row['gender'],
            row['level']
            )
        cur.execute(command)

    # # insert songplay records
    dfs = df.reset_index(drop=False)
    dfs = dfs[['index','ts','userId','level','sessionId','location','length','song','artist']]
    dfs['userId'] = pandas.to_numeric(dfs['userId'])

    for index,row in dfs.iterrows():
        # print(row)
        command = "SELECT song_id, artist_id FROM songs WHERE title='{}'".format(
            row['song'].replace("\'",'\"')
            )
        cur.execute(command)
        results = cur.fetchone()

        if results:
            songid, artistid = results
        else:
            songid, artistid = None, None

        command = 'INSERT INTO songplays (start_time,user_id,level,song_id,artist_id,session_id,location,length,song_name,artist_name) VALUES ({},{},"{}","{}","{}",{},"{}",{},"{}","{}") ON CONFLICT (songplay_id) DO NOTHING'.format(
                row['ts'],
                row['userId'],
                row['level'],
                songid,
                artistid,
                row['sessionId'],
                row['location'],
                row['length'],
                row['song'].replace("\"",'\''),
                row['artist'].replace("\"",'\'')
                )
        cur.execute(command)


def process_data(cur, con, filepath, func):
    # get all files matching extension from directory
    all_files = []
    for root, dirs, files in os.walk(filepath):
        files = glob.glob(os.path.join(root,'*.json'))
        for f in files :
            all_files.append(os.path.abspath(f))

    # get total number of files found
    num_files = len(all_files)
    print('{} files found in {}'.format(num_files, filepath))

    # iterate over files and process
    for i, datafile in enumerate(all_files, 1):
        func(cur, datafile)
        con.commit()
        print('{}/{} files processed.'.format(i, num_files))


def main():
    con = sqlite3.connect('sparkify.db')
    cur = con.cursor()

    process_data(cur, con, filepath='data/song_data', func=process_song_file)
    process_data(cur, con, filepath='data/log_data', func=process_log_file)

    cur.close()
    con.close()


if __name__ == "__main__":
    main()