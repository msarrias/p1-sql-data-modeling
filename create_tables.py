import sqlite3
import os
from sql_queries import create_table_queries, create_views_queries


def create_database():
    """
    - Creates and connects to the sparkifydb
    - Returns the connection and cursor to sparkifydb
    """

    # drop existing db
    try:
        os.remove('sparkify.db')
    except OSError:
        pass

    # create/connect sqlite db
    con = sqlite3.connect('sparkify.db')
    cur = con.cursor()
    
    return cur, con


def create_tables(cur, con):
    """
    Creates each table using the queries in `create_table_queries` list. 
    """
    for query in create_table_queries:
        cur.execute(query)
        con.commit()

def create_views(cur, con):
    """
    Creates each table using the queries in `create_table_queries` list. 
    """
    for query in create_views_queries:
        cur.execute(query)
        con.commit()

def main():
    """
    - Drops (if exists) and Creates the sparkify database. 
    - Establishes connection with the sparkify database and gets
    cursor to it.  
    - Creates all tables needed. 
    - Finally, closes the connection. 
    """
    cur, con = create_database()
    
    create_tables(cur, con)
    create_views(cur, con)


    cur.close()
    con.close()


if __name__ == "__main__":
    main()