# CREATE TABLES

songplay_table_create = """
CREATE TABLE IF NOT EXISTS songplays (
    songplay_id integer, 
    start_time integer, 
    user_id integer, 
    level text, 
    song_id text,
    artist_id text,
    session_id integer,
    location text,
    length real,
    song_name text,
    artist_name text,
	PRIMARY KEY("songplay_id" AUTOINCREMENT)
    )
"""

user_table_create = """
CREATE TABLE IF NOT EXISTS users (
    user_id integer, 
    first_name text, 
    last_name text, 
    gender text, 
    level text,
	PRIMARY KEY("user_id")
    )
"""

song_table_create = """
CREATE TABLE IF NOT EXISTS songs (
    song_id text, 
    title text, 
    artist_id text, 
    year integer, 
    duration real,
	PRIMARY KEY("song_id")
    )
"""

artist_table_create = """
CREATE TABLE IF NOT EXISTS artists (
    artist_id text, 
    name text, 
    location text, 
    lat real, 
    lon real,
	PRIMARY KEY("artist_id")
    )
"""

time_table_create = """
CREATE TABLE IF NOT EXISTS time (
    start_time integer, 
    day integer, 
    month integer, 
    year integer, 
    hour integer, 
    week integer, 
    weekday integer,
	PRIMARY KEY("start_time")
    )
"""

# CREATE VIEWS

view_top_users = """
CREATE VIEW "top_users" AS 
SELECT songplays.user_id, first_name, last_name, gender, songplays.level, SUM(length)/3600 
FROM (songplays JOIN users ON users.user_id=songplays.user_id) 
GROUP BY songplays.user_id
ORDER BY SUM(length)/3600 DESC
"""

view_top_songs = """
CREATE VIEW "top_songs" AS 
SELECT song_name, artist_name, COUNT(*) 
FROM songplays
GROUP BY song_name
ORDER BY COUNT(*) DESC
"""

view_top_artists = """
CREATE VIEW "top_artists" AS SELECT artist_name, COUNT(*) 
FROM songplays
GROUP BY artist_name
ORDER BY COUNT(*) DESC
"""


# QUERY LISTS

create_table_queries = [songplay_table_create, user_table_create, song_table_create, artist_table_create, time_table_create]
create_views_queries = [view_top_users, view_top_songs, view_top_artists]