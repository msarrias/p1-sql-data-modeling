# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 16:59:21 2022

@author: miquel.sarrias.ext
"""


import pandas
import os
import glob
import json

filepath = r'C:/Users/Miquel.sarrias.ext/OneDrive - SUEZ/Documentos/Management/Formacions/data/nanodegree udacity/p1-sql-data-modeling/data/song_data'

# get all files matching extension from directory
all_files = []
for root, dirs, files in os.walk(filepath):
    files = glob.glob(os.path.join(root,'*.json'))
    for f in files :
        all_files.append(os.path.abspath(f))

# get total number of files found
num_files = len(all_files)
print('{} files found in {}'.format(num_files, filepath))


# load into pandas
docs = []
for file in all_files:
    print(file[-30:])
    with open(file, 'r') as f:
        d = json.load(f)
    docs.append(d)
df = pandas.DataFrame(docs)


#%% log_data

filepath = r'C:/Users/Miquel.sarrias.ext/OneDrive - SUEZ/Documentos/Management/Formacions/data/nanodegree udacity/p1-sql-data-modeling/data/log_data'

# get all files matching extension from directory
all_files = []
for root, dirs, files in os.walk(filepath):
    files = glob.glob(os.path.join(root,'*.json'))
    for f in files :
        all_files.append(os.path.abspath(f))

# get total number of files found
num_files = len(all_files)
print('{} files found in {}'.format(num_files, filepath))

docs = []
for file in all_files:
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        j = json.loads(line)
        docs.append(j)

df = pandas.DataFrame(docs)
df = df[df['page']=='NextSong']
df['ts2'] = df['ts']*1000000
df['time'] = pandas.to_datetime(df['ts2'])
dft = df[['ts','time']]
dft['hour'] = df['time'].dt.hour
dft['day'] = df['time'].dt.day
dft['month'] = df['time'].dt.month
dft['year'] = df['time'].dt.year
dft['week'] = df['time'].dt.week
dft['weekday'] = df['time'].dt.weekday


# load user table
dfu = df[['userId','firstName','lastName','gender','level']]
dfu = dfu.drop_duplicates()
dfu['userId'] = pandas.to_numeric(dfu['userId'])





# insert user records
for i, row in user_df.iterrows():
    cur.execute(user_table_insert, row)

# insert songplay records
for index, row in df.iterrows():
    
    # get songid and artistid from song and artist tables
    cur.execute(song_select, (row.song, row.artist, row.length))
    results = cur.fetchone()
    
    if results:
        songid, artistid = results
    else:
        songid, artistid = None, None

    # insert songplay record
    songplay_data = 1
    cur.execute(songplay_table_insert, songplay_data)